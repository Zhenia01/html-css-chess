FROM node:alpine

COPY index.html ./
COPY sass ./sass
COPY server.js ./
COPY package*.json ./

RUN npm install
RUN npm run sass
CMD node server.js

EXPOSE 8082:8082
