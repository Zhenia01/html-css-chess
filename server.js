var express = require('express');
var app = express();
const PORT = process.env.PORT || 8082;

app.use('/', express.static(__dirname));

app.listen(PORT, function () {
  console.log(`Check localhost:${PORT}`);
});
